# Description #
<https://en.wikipedia.org/wiki/Nuclear_magnetic_resonance_spectroscopy>

# Examples #

H-NMR spectra were recorded on a Bruker AV(II) 600 NMR spectrometer operating at 600.13 MHz at 300 K. Each 1H NMR spectrum was recorded with a pulse repetition rate of 4.73 s and NS = 8 giving a total acquisition time for each spectrum of less than 40 s to minimise the effects of changes in the sample during measurement.
> Adams, D. J., Butler, M. F., Frith, W. J., Kirkland, M., Mullen, L., & Sanderson, P. (2009). A new method for maintaining homogeneity during liquid–hydrogel transitions using low molecular weight hydrogelators. *Soft Matter*, 5(9), 1856-1862.

